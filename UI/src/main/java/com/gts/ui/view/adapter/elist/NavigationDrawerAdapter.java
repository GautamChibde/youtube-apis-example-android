package com.gts.ui.view.adapter.elist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.gts.ui.view.ViewFactory;
import com.gts.ui.view.model.Link;
import com.gts.ui.view.model.LinkGroup;

import java.util.List;

public class NavigationDrawerAdapter extends BaseExpandableListAdapter {

    private final List<LinkGroup> linkGroups;
    protected final LayoutInflater inflater;
    protected final Context context;

    public NavigationDrawerAdapter(Context context, List<LinkGroup> linkGroups) {
        this.linkGroups = linkGroups;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = new TextView(context);
            convertView.setLayoutParams(
                    new ListView.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT
                    )
            );
        }
        initGroupView(convertView, groupPosition);
        return convertView;
    }


    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView,
                             ViewGroup parent) {
        if (convertView == null) {
            convertView = ViewFactory.getDrawerItemView(context, 18, 32);
        }
        initChildView(convertView, groupPosition, childPosition, isLastChild);
        return convertView;
    }

    @Override
    public int getGroupCount() {
        return linkGroups.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return linkGroups.get(groupPosition).getLinks().size();
    }

    @Override
    public LinkGroup getGroup(int groupPosition) {
        return linkGroups.get(groupPosition);
    }

    @Override
    public Link getChild(int groupPosition, int childPosition) {
        return linkGroups.get(groupPosition).getLinks().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        int offset = groupPosition * 100;
        return childPosition + offset;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    protected void initGroupView(View convertView, int groupPosition) {
        ((TextView) convertView).setText(getGroup(groupPosition).getName());
    }

    protected void initChildView(View convertView, int groupPosition,
                                 int childPosition, boolean isLastChild) {
        Link link = getChild(groupPosition, childPosition);
        TextView textView = (TextView) convertView;
        textView.setText(link.getName());
        textView.setCompoundDrawablesWithIntrinsicBounds(link.getImageId(), 0, 0, 0);
    }

}

package com.gts.ui.view.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.ViewGroup;

import com.gts.util.Constant;

import java.io.Serializable;

public class ConfirmDialog extends AbstractDialogFragment {

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
            );
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final int themeId = getArguments().getInt(Constant.THEME);
        final ActionListener listener =
                (ActionListener) getArguments().getSerializable(
                        Constant.LISTENER
                );
        final String msg = getArguments().getString(Constant.MESSAGE);
        AlertDialog.Builder builder;
        if (themeId != 0) {
            builder = new AlertDialog.Builder(
                    getActivity(),
                    themeId
            );
        } else {
            builder = new AlertDialog.Builder(getActivity());
        }
        builder.setMessage(msg != null ? msg : "")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (listener != null) {
                            listener.onAccept();
                        }
                        dismiss();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (listener != null) {
                            listener.onReject();
                        }
                        dismiss();
                    }
                });
        return builder.create();
    }

    public static abstract class ActionListener implements Serializable {
        private static final Long serialVersionUID = 1L;

        public void onAccept() {
        }

        public void onReject() {
        }
    }
}

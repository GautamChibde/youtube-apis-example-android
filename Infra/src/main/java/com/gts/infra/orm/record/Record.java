package com.gts.infra.orm.record;

import java.io.Serializable;

public interface Record extends Serializable {

    Long getId();

    void setId(Long id);

    void save();

    boolean isValid();

    boolean isArchived();

    boolean isNew();

    boolean isMutated();

    boolean isActive();

}

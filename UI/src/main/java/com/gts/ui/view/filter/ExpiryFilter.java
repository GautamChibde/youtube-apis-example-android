package com.gts.ui.view.filter;

import android.text.InputFilter;
import android.text.Spanned;

public class ExpiryFilter implements InputFilter {

    @Override
    public CharSequence filter(CharSequence source, int start,
                               int end, Spanned dest, int dStart, int dEnd) {
        try {
            String newVal = dest.toString().substring(0, dStart)
                    + dest.toString().substring(dEnd, dest.toString().length());
            newVal = newVal.substring(0, dStart)
                    + source.toString()
                    + newVal.substring(dStart, newVal.length());
            String[] splits = new String[2];
            if (newVal.contains("/")) {
                splits = newVal.split("/");
            } else {
                splits[0] = newVal;
            }
            if ((splits.length == 1
                    && monthValid(splits[0])) ||
                    (splits.length == 2 &&
                            monthValid(splits[0]) &&
                            yearValid(splits[1]))) {
                return null;
            }
        } catch (NumberFormatException nfe) {
            return null;
        }
        return "";
    }

    private boolean monthValid(String val) {
        return val.length() == 3 || isInRange(0, 12, Integer.parseInt(val));
    }

    private boolean yearValid(String val) {
        return isInRange(0, 99, Integer.parseInt(val));
    }

    private boolean isInRange(int min, int max, int val) {
        return val >= min && val <= max;
    }
}

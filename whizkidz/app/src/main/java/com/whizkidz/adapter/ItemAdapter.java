package com.whizkidz.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gts.util.Constant;
import com.gts.util.PlatformUtil;
import com.whizkidz.R;
import com.whizkidz.activity.VideoStreamerActivity;
import com.whizkidz.model.Data;

import java.util.List;

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.itemHolder> {

    private Context context;
    private List<Data> items;

    public ItemAdapter(Context context, List<Data> itemList) {
        this.context = context;
        this.items = itemList;
    }

    @Override
    public itemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new itemHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_data, parent, false));
    }

    @Override
    public void onBindViewHolder(itemHolder holder, final int position) {
        if (items.get(position).getTitle().length() > 45) {
            holder.title.setText(
                    String.valueOf(items.get(position).getTitle().substring(0, 45) + "..."));
        } else {
            holder.title.setText(items.get(position).getTitle());
        }
        holder.lv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, VideoStreamerActivity.class);
                i.putExtras(PlatformUtil.bundleSerializable(Constant.MODEL, items.get(position)));
                context.startActivity(i);
            }
        });
        holder.lv.setBackgroundResource(R.drawable.cart);
        String ThumbUrl = "http://img.youtube.com/vi/"
                + PlatformUtil.extractYTId(items.get(position).getUrl())
                + "/2.jpg";
        PlatformUtil.setImagePicasso(context,
                ThumbUrl,
                holder.image,
                R.drawable.nothumb,
                R.drawable.error);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class itemHolder extends RecyclerView.ViewHolder {
        private TextView title;
        private ImageView image;
        private RelativeLayout lv;

        public itemHolder(View v) {
            super(v);
            title = (TextView) v.findViewById(R.id.title);
            lv = (RelativeLayout) v.findViewById(R.id.card);
            image = (ImageView) v.findViewById(R.id.image);
        }
    }
}
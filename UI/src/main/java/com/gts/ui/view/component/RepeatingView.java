package com.gts.ui.view.component;

import android.content.Context;
import android.database.DataSetObserver;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.gts.ui.view.adapter.list.AbstractListAdapter;


public class RepeatingView extends LinearLayout {

    protected AbstractListAdapter<?> adapter;

    private DataSetObserver observer = new DataSetObserver() {
        @Override
        public void onChanged() {
            requestLayout();
        }

        @Override
        public void onInvalidated() {
            requestLayout();
        }
    };

    public RepeatingView(Context context) {
        this(context, null);
    }

    public RepeatingView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public AbstractListAdapter getAdapter() {
        return adapter;
    }

    public <T> void setAdapter(AbstractListAdapter<T> adapter) {
        this.adapter = adapter;
        adapter.registerDataSetObserver(observer);
        requestLayout();
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        setOrientation(VERTICAL);
        if (adapter == null) {
            return;
        }
        if (adapter.getCount() > 0) {
            removeAllViews();
            int position = 0;
            while (position < adapter.getCount()) {
                addView(adapter.getView(position, null, this));
                position++;
            }
        }
        invalidate();
    }


}

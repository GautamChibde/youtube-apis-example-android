package com.gts.ui.view.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;

import com.gts.infra.orm.base.NamedRecord;
import com.gts.ui.R;
import com.gts.ui.view.adapter.list.AbstractAutoCompleteAdapter;
import com.gts.util.PlatformUtil;

import java.util.List;

public abstract class RecordFinder<T extends NamedRecord>
        extends AutoCompleteTextView {

    public RecordFinder(Context context) {
        super(context);
    }

    public RecordFinder(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RecordFinder(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        requestFocus();
        setCompoundDrawablesWithIntrinsicBounds(
                0, 0, R.drawable.ic_done_black_24dp, 0
        );
        setThreshold(1);
        setAdapter(new AbstractAutoCompleteAdapter<T>(getContext()) {

            @Override
            protected List<T> performFiltering(CharSequence constraint) {
                return RecordFinder.this.filter(constraint);
            }

            @Override
            protected String getLabel(T aValue) {
                return aValue.getName();
            }
        });
        setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {

                    if (event.getRawX() >= (RecordFinder.this.getRight() - 60)) {
                        onSubmit(null);
                        return true;
                    }
                }
                return false;
            }
        });

        setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                onSubmit((T) parent.getAdapter().getItem(position));
            }
        });
    }

    protected abstract void onSubmit(T entity);

    protected abstract List<T> filter(CharSequence constraint);

}

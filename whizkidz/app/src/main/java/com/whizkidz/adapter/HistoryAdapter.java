package com.whizkidz.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gts.util.Constant;
import com.gts.util.PlatformUtil;
import com.whizkidz.R;
import com.whizkidz.activity.VideoStreamerActivity;
import com.whizkidz.model.History;

import java.util.List;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.HistoryHolder> {

    private List<History> items;
    private Context context;

    public HistoryAdapter(Context context, List<History> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public HistoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new HistoryHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_history, parent, false));
    }

    @Override
    public void onBindViewHolder(HistoryHolder holder, final int position) {
        holder.title.setText(items.get(position).getItem().getTitle());
        holder.viewed.setText(String.valueOf("Viewed :" + items.get(position).getCount()));
        String ThumbUrl = "http://img.youtube.com/vi/"
                + PlatformUtil.extractYTId(items.get(position).getItem().getUrl())
                + "/2.jpg";
        PlatformUtil.setImagePicasso(context,
                ThumbUrl,
                holder.image,
                R.drawable.nothumb,
                R.drawable.error);
        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, VideoStreamerActivity.class);
                i.putExtras(PlatformUtil.bundleSerializable(
                        Constant.MODEL,
                        items.get(position).getItem()));
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class HistoryHolder extends RecyclerView.ViewHolder {
        private TextView title, viewed;
        private ImageView image;
        private LinearLayout card;

        public HistoryHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            viewed = (TextView) itemView.findViewById(R.id.views);
            image = (ImageView) itemView.findViewById(R.id.image);
            card = (LinearLayout) itemView.findViewById(R.id.card);
        }
    }
}

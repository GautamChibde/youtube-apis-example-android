package com.gts.ui.view.adapter.pager;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.AsyncTask;
import android.support.v13.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractPagerAdapter<T> extends FragmentStatePagerAdapter {

    protected List<T> pages = new ArrayList<>();

    public AbstractPagerAdapter(FragmentManager fm) {
        super(fm);
        new LoadTask().execute();
    }

    @Override
    public Fragment getItem(int position) {
        return getFragment(pages.get(position));
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return getTitle(pages.get(position));
    }

    protected abstract CharSequence getTitle(T t);

    protected abstract Fragment getFragment(T t);

    protected abstract List<T> getData();

    @Override
    public int getCount() {
        return pages.size();
    }

    private class LoadTask extends AsyncTask<Void, Void, List<T>> {

        @Override
        protected List<T> doInBackground(Void... params) {
            return getData();
        }

        @Override
        protected void onPostExecute(List<T> ts) {
            if (ts != null && ts.size() > 0) {
                pages.clear();
                pages.addAll(ts);
                notifyDataSetChanged();
            }
        }
    }
}

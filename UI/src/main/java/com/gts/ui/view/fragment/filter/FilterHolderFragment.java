package com.gts.ui.view.fragment.filter;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gts.ui.view.ViewFactory;
import com.gts.ui.view.adapter.pager.FilterPagerAdapter;
import com.gts.ui.view.controller.UpPanelManager;
import com.gts.ui.view.fragment.AbstractFragment;

import java.util.ArrayList;
import java.util.List;

public class FilterHolderFragment<T> extends AbstractFragment {

    protected List<AbstractFilterFragment<T>> filterFragments = new ArrayList<>();
    protected FilterPagerAdapter filterAdapter;

    protected Toolbar toolbar;
    protected ViewPager viewPager;
    protected TextView filtersLabel;
    protected TabLayout tabLayout;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {

        /**************** TOOLBAR *****************/
        LinearLayout.LayoutParams toolbarChildrenParams = getLinearLayoutParams();
        //Spinner sortSpinner = new Spinner(getActivity());
        //sortSpinner.setLayoutParams(toolbarChildrenParams);
        //sortSpinner.setAdapter(getSortAdapter());
        filtersLabel = new TextView(getActivity());
        filtersLabel.setLayoutParams(toolbarChildrenParams);
        filtersLabel.setTextSize(18);
        filtersLabel.setTypeface(Typeface.SERIF);
        LinearLayout toolbarChildren = ViewFactory.getHorizontalFull(getActivity());
        toolbarChildren.addView(filtersLabel);
        //toolbarChildren.addView(sortSpinner);

        toolbar = new Toolbar(getActivity());
        toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                post(new UpPanelManager.TogglePanelEvent());
            }
        });
        toolbar.addView(toolbarChildren);

        /**************** PAGER WITH TABS *****************/
        tabLayout = new TabLayout(getActivity());
        // TODO viewPager = ViewFactory.getViewPager(getActivity(), getFilterAdapter());
        tabLayout.setupWithViewPager(viewPager);

        /**************** ROOT *****************/
        LinearLayout root = ViewFactory.getRootLinearLayout(getActivity());
        root.addView(toolbar);
        root.addView(tabLayout);
        root.addView(viewPager);

        return root;
    }

    @NonNull
    private LinearLayout.LayoutParams getLinearLayoutParams() {
        LinearLayout.LayoutParams toolbarChildrenParams = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                1
        );
        toolbarChildrenParams.gravity = Gravity.CENTER_VERTICAL;
        return toolbarChildrenParams;
    }

//    protected FragmentStatePagerAdapter getFilterAdapter() {
//        return filterAdapter =
//                new FilterPagerAdapter<>(getFragmentManager(),
//                        filterFragments);
//    }

}

package com.gts.infra.net.http;

import android.util.Log;

import com.google.gson.reflect.TypeToken;
import com.gts.infra.App;
import com.gts.infra.ReflectionUtil;
import com.gts.infra.auth.Auth;
import com.gts.infra.data.SearchRequest;
import com.gts.infra.net.http.api.error.VolleyError;
import com.gts.infra.net.http.api.request.AuthGsonRequest;
import com.gts.infra.net.http.api.request.Request;
import com.gts.infra.net.http.api.response.Response;
import com.gts.infra.net.http.impl.DefaultRetryPolicy;
import com.gts.infra.orm.base.BaseEntity;
import com.gts.infra.orm.repo.EntityRepo;
import com.gts.util.Constant;

import java.lang.reflect.Type;
import java.util.concurrent.TimeUnit;

import de.greenrobot.event.EventBus;

@SuppressWarnings("unused")
public class Resource {

    private static final String TAG = Resource.class.getName();

    public static <Rq, Rs> void post(App app, String resourceUri,
                                     Rq body, Response.Listener<Rs> responseListener,
                                     Response.ErrorListener errorListener, Type type,
                                     String token) {
        Request<?> rq = new AuthGsonRequest<>(
                Request.Method.POST,
                app.getBaseUri() + resourceUri,
                body,
                responseListener,
                errorListener,
                type,
                token
        );

        if (Constant.CONTEXT_SIGNUP.equals(resourceUri)
                || Constant.CONTEXT_TOKEN.equals(resourceUri)) {
            rq.setRetryPolicy(new DefaultRetryPolicy(
                    (int) TimeUnit.SECONDS.toMillis(20),
                    0,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));
        }
        app.getQueue().add(rq);
    }

    public static <Rs> void get(App app, String resourceUri,
                                Response.Listener<Rs> responseListener,
                                Response.ErrorListener errorListener, Type type,
                                String token) {
        app.getQueue().add(new AuthGsonRequest<>(
                Request.Method.GET,
                app.getBaseUri() + resourceUri,
                null,
                responseListener,
                errorListener,
                type,
                token));
    }

    public static <Rq, Rs> void postAsync(App app, String resourceUri, final int requestId,
                                          Type type, Rq body, String token) {
        post(app, resourceUri, body,
                new Response.Listener<Rs>() {
                    @Override
                    public void onResponse(Rs response) {
                        EventBus.getDefault().post(
                                new HttpRequestComplete<>(
                                        requestId,
                                        true,
                                        null,
                                        response
                                )
                        );
                    }
                },
                newErrorListener(requestId),
                type,
                token);
    }

    public static <Rs> void getAsync(App app, String resourceUri,
                                     final int requestId,
                                     Type type, String token) {
        get(app, resourceUri,
                new Response.Listener<Rs>() {
                    @Override
                    public void onResponse(Rs response) {
                        EventBus.getDefault().post(
                                new HttpRequestComplete<>(requestId, true, null, response));
                    }
                },
                newErrorListener(requestId),
                type,
                token);
    }

    /*******************************
     * Reusable Standard requests
     *******************************/

    public static void getClientToken(App app) {
        postAsync(
                app,
                "/api/token",
                Constant.TOKEN_REQUEST_CODE,
                new TypeToken<Auth>() {
                }.getType(),
                new Auth(app.getAppCredentials()[0],
                        app.getAppCredentials()[1],
                        Constant.GRANT_TYPE_CLIENT_CREDENTIALS),
                null
        );
    }

    public static void getUserToken(App app, String resourceUri,
                                    Auth authRequest, Type type) {
        postAsync(
                app,
                resourceUri,
                Constant.AUTH_REQUEST_CODE,
                type,
                authRequest,
                app.getClientToken()
        );
    }

    public static void placeOrder(App app, Type type, Object order, String token) {
        postAsync(
                app,
                "/api/order",
                Constant.PLACE_ORDER_REQUEST_CODE,
                type,
                order,
                token
        );
    }

    public static void sync(App app, Class clazz, SearchRequest request) {
        sync(app, clazz, request, app.getClientToken());
    }

    public static void syncUser(App app, Class clazz, SearchRequest request) {
        sync(app, clazz, request, app.getUserToken());
    }

    public static void sync(App app, Class clazz,
                            SearchRequest request, String authToken) {
        Resource.postAsync(
                app,
                Constant.SEARCH_RESOURCE_URI,
                (Integer) ReflectionUtil.getValue(clazz, "REQUEST_CODE"),
                (Type) ReflectionUtil.getValue(clazz, "TYPE_LIST"),
                request,
                authToken
        );
    }

    public static void sync(App app, Class<? extends BaseEntity> clazz) {
        sync(app, clazz, getDefaultSearchRequest(clazz));
    }

    public static void saveToServer(App app, Object entity) {
        Class clazz = entity.getClass();
        Resource.postAsync(
                app,
                (String) ReflectionUtil.getValue(clazz, "RESOURCE_URI"),
                (Integer) ReflectionUtil.getValue(clazz, "REQUEST_CODE"),
                (Type) ReflectionUtil.getValue(clazz, "TYPE"),
                entity,
                app.getClientToken()
        );
    }

    private static SearchRequest getDefaultSearchRequest(Class<? extends BaseEntity> clazz) {
        return new SearchRequest(
                (String) ReflectionUtil.getValue(clazz, "DOC"),
                EntityRepo.getLastUpdatedStamp(clazz)
        );
    }

    /**
     * *********************************************************************
     */

    private static <Rs> Response.ErrorListener newErrorListener(final int requestId) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                EventBus.getDefault().post(
                        new HttpRequestComplete<Rs>(
                                requestId,
                                false,
                                error.getMessage(),
                                null
                        )
                );
            }
        };
    }


    public static class HttpRequestComplete<T> {
        private final int requestId;
        private final boolean successful;
        private final String message;
        private final T response;

        public HttpRequestComplete(int requestId, boolean success,
                                   String message, T response) {
            this.requestId = requestId;
            this.successful = success;
            this.message = message;
            this.response = response;
        }

        @Override
        public String toString() {
            return "HttpRequestComplete{" +
                    "requestId=" + requestId +
                    ", response =" + response +
                    ", successful=" + successful +
                    ", message='" + message + '\'' +
                    '}';
        }


        public boolean isSuccessful() {
            return successful;
        }

        public String getMessage() {
            return message;
        }

        public int getRequestId() {
            return requestId;
        }

        public T getResponse() {
            return response;
        }
    }

}
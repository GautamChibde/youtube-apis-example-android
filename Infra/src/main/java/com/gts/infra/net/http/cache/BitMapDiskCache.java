package com.gts.infra.net.http.cache;

import android.graphics.Bitmap;
import android.util.Log;

import com.gts.infra.VolleyLog;
import com.gts.infra.net.http.impl.ImageLoader;

import java.io.File;

public class BitMapDiskCache
        implements ImageLoader.ImageCache {

    private final BitmapLruCache bitmapLruCache =
            new BitmapLruCache(10000);

    /**
     * The root directory to use for the cache.
     */
    private final File mRootDirectory;

    public BitMapDiskCache(File mRootDirectory) {
        this.mRootDirectory = mRootDirectory;
        if (!mRootDirectory.exists()) {
            if (!mRootDirectory.mkdirs()) {
                VolleyLog.e(
                        "Unable to create cache dir %s",
                        mRootDirectory.getAbsolutePath()
                );
            }
            return;
        }

//        File[] files = mRootDirectory.listFiles();
//        if (files != null) {
//            for (File file : files) {
//                com.gts.infra.Log.d("file %s", file);
//                bitmapLruCache.putBitmap(
//                        file.getName(),
//                        BitmapFactory.decodeFile(
//                                file.getAbsolutePath()
//                        )
//                );
//            }
//        }
    }


    @Override
    public Bitmap getBitmap(String url) {
        if (url.indexOf("static/") > 0) {
            String key = url.substring(
                    url.indexOf("static/") + 7,
                    url.length()
            );
            Log.i("Cache", "url to get" + url + "; key: " + key);
            if (key.length() > 0) {

                Bitmap bitmap = bitmapLruCache.getBitmap(key);
//                if (bitmap == null) {
//                    bitmap = BitmapFactory.decodeFile(
//                            mRootDirectory + "/" + key
//                    );
//                    if (bitmap != null) {
//                        bitmapLruCache.putBitmap(
//                                key,
//                                bitmap
//                        );
//                    }
//                }
                if(bitmap != null){
                    Log.i("Cache", "Got image in cache");
                }
                return bitmap;
            }
        }
        return null;
    }

    @Override
    public void putBitmap(String url, Bitmap b) {
        if (url.indexOf("static/") > 0) {
            String key = url.substring(
                    url.indexOf("static/") + 7,
                    url.length()
            );
            Log.i("Cache", "url to get" + url + "; key: " + key);
            bitmapLruCache.putBitmap(key, b);
        }
    }
}

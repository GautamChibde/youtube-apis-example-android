package com.gts.infra.auth;

import java.io.Serializable;

@SuppressWarnings("unused")
public class Auth implements Serializable {

    private static final Long serialVersionUID = 1L;

    protected String name;
    protected String client_id;
    protected String client_secret;
    protected String username;
    protected String password;
    protected String grant_type;
    protected String idType;
    protected String access_token;
    protected String rol;
    protected Long user_id;

    public Auth(String client_id, String client_secret, String grant_type) {
        this.client_id = client_id;
        this.client_secret = client_secret;
        this.grant_type = grant_type;
    }

    public Auth(String client_id, String client_secret, String name,
                String idType, String username, String password,
                String grant_type) {
        this.name = name;
        this.client_id = client_id;
        this.client_secret = client_secret;
        this.idType = idType;
        this.username = username;
        this.password = password;
        this.grant_type = grant_type;
    }

    @Override
    public String toString() {
        return "Auth{" +
                "client_id=" + client_id +
                ", client_secret=" + client_secret +
                ", idType='" + idType +
                ", username=" + username +
                ", password=" + password +
                ", grant_type=" + grant_type +
                ", access_token=" + access_token +
                ", rol=" + rol +
                ", userId=" + user_id +
                ", name=" + name +
                '}';
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public String getIdType() {
        return idType;
    }

    public Long getUser_id() {
        return user_id;
    }
}

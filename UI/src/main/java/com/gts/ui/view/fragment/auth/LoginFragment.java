package com.gts.ui.view.fragment.auth;

import android.text.TextUtils;
import android.view.View;

import com.gts.ui.R;
import com.gts.util.PlatformUtil;

public class LoginFragment extends AbstractAuthFragment {

    protected int getLayoutId() {
        return R.layout.fragment_login;
    }

    @Override
    protected void initView(View v) {
        super.initView(v);
        v.findViewById(R.id.tvSignUp).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replaceWithFragment(
                        SignUpFragment.class,
                        getArguments(),
                        true
                );
            }
        });
        v.findViewById(R.id.tvForgotPassword).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replaceWithFragment(
                        ForgotPasswordFragment.class,
                        getArguments(),
                        true
                );
            }
        });
    }

    protected void onSubmit() {
        authManager.login(
                etUsername.getText().toString(),
                etPassword.getText().toString()
        );
    }

    protected boolean dataValid() {
        if (!PlatformUtil.isValidEmail(etUsername.getText())) {
            etUsername.setError("Email invalid");
            return false;
        } else {
            etUsername.setError(null);
        }
        if (TextUtils.isEmpty(etPassword.getText())) {
            etPassword.setError("Please enter your password");
            return false;
        } else {
            etPassword.setError(null);
        }
        return true;
    }

    @Override
    protected String getTitle() {
        return "Login";
    }

}

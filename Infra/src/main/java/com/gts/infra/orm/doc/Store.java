package com.gts.infra.orm.doc;

import com.gts.infra.App;
import com.gts.infra.ReflectionUtil;
import com.gts.infra.auth.Auth;
import com.gts.infra.data.SearchRequest;
import com.gts.infra.net.NetworkManager;
import com.gts.infra.net.http.Resource;
import com.gts.infra.orm.record.Record;
import com.gts.util.Constant;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

/**
 * Communicating with a network record store
 */
// TODO Token Renewal
public class Store {

    private App app;

    private static final Map<Class<? extends Record>, RecordMeta> requestMap = new HashMap<>();

    private Queue<SyncRequest<? extends Record>> requestQueue = new LinkedList<>();

    public Store(App app) {
        this.app = app;
    }

    private void fetchToken() {
        Resource.getClientToken(app);
    }

    public void sync(SyncRequest<? extends Record> sRequest) {
        Class<? extends Record> clazz = sRequest.getClazz();
        if (!app.getNetworkManager().isConnected()) {
            requestQueue.add(sRequest);
            app.getBus().post(new NoNetworkEvent());
            return;
        }
        if (!hasToken()) {
            requestQueue.add(sRequest);
            fetchToken();
            return;
        }
        if (sRequest.isUserRequest() && !isLoggedIn()) {
            requestQueue.add(sRequest);
            app.getBus().post(new NotLoggedInEvent());
            return;
        }
        RecordMeta meta = getMeta(clazz);
        sRequest.getRequest().setDoc(meta.getDoc());
        Resource.postAsync(
                app,
                Constant.SEARCH_RESOURCE_URI,
                meta.getRequestId(),
                meta.getTypeList(),
                sRequest.getRequest(),
                sRequest.isUserRequest() ?
                        app.getUserToken() :
                        app.getClientToken()
        );
        meta.setSyncDone(false);
    }

    public void save(Record doc) {
        if (null != doc) {
            if (doc.isMutated() || doc.isNew()) {
                RecordMeta meta = getMeta(doc.getClass());
                Resource.postAsync(
                        app,
                        meta.getResourceUri(),
                        meta.getRequestId(),
                        meta.getTypeObject(),
                        doc,
                        app.getUserToken()
                );
            }
        }
    }

    @SuppressWarnings("unused")
    public void onEvent(NetworkManager.NetworkStateChangedEvent event) {
        requestPendingSync();
    }

    @SuppressWarnings("unchecked,unused")
    public void onEventAsync(Resource.HttpRequestComplete event) {
        if (Constant.TOKEN_REQUEST_CODE == event.getRequestId()) {
            if (event.getResponse() instanceof Auth) {
                String accessToken = ((Auth) event.getResponse()).getAccess_token();
                if (null != accessToken) {
                    app.saveToPref(Constant.KEY_CLIENT_TOKEN, accessToken);
                    requestPendingSync();
                } else {
                    app.getBus().post(new SyncFailedEvent("Null response"));
                }
            }else{
                app.getBus().post(new SyncFailedEvent("Token null"));
            }
        } else {
            if (requestMap.containsValue(new RecordMeta(event.getRequestId()))) {
                RecordMeta classMeta = null;
                for (RecordMeta meta : requestMap.values()) {
                    if (meta.getRequestId() == event.getRequestId()) {
                        classMeta = meta;
                        meta.setSyncDone(true);
                    }
                }
                if (event.getResponse() instanceof List) {
                    for (Record doc : (List<? extends Record>) event.getResponse()) {
                        doc.save();
                    }
                    app.getBus().post(new RecordSyncedEvent(
                            classMeta != null ? classMeta.getClazz() : null
                    ));
                } else if (event.getResponse() instanceof Record) {
                    Record toSave = (Record) event.getResponse();
                    if (toSave != null) {
                        toSave.save();
                        app.getBus().post(new RecordSavedEvent(toSave));
                    }
                }
            }
        }
    }

    private void requestPendingSync() {
        SyncRequest<?> syncRequest = requestQueue.poll();
        while (syncRequest != null) {
            sync(syncRequest);
            syncRequest = requestQueue.poll();
        }
    }

    private boolean isLoggedIn() {
        return app.getUserToken() != null;
    }

    private boolean hasToken() {
        return app.getClientToken() != null;
    }

    private RecordMeta getMeta(Class<? extends Record> clazz) {
        RecordMeta meta;
        if (requestMap.containsKey(clazz)) {
            meta = requestMap.get(clazz);
        } else {
            meta = docToMeta(clazz);
            requestMap.put(clazz, meta);
        }
        return meta;
    }

    private RecordMeta docToMeta(Class<? extends Record> clazz) {
        RecordMeta recordMeta = new RecordMeta(
                ReflectionUtil.getInteger(
                        clazz, RecordMeta.REQUEST_CODE
                )
        );
        recordMeta.setClazz(clazz);
        recordMeta.setResourceUri(
                ReflectionUtil.getString(
                        clazz, RecordMeta.RESOURCE_URI
                )
        );
        recordMeta.setTypeList(
                ReflectionUtil.getType(
                        clazz, RecordMeta.TYPE_LIST
                )
        );
        recordMeta.setTypeObject(
                ReflectionUtil.getType(
                        clazz, RecordMeta.TYPE
                )
        );
        recordMeta.setDoc(
                ReflectionUtil.getString(
                        clazz, RecordMeta.DOC
                )
        );
        return recordMeta;
    }

    public static class RecordSavedEvent<T extends Record> {
        private final T document;

        public RecordSavedEvent(T document) {
            this.document = document;
        }

        public T getRecord() {
            return document;
        }
    }

    public static class RecordSyncedEvent<T extends Record> {

        private final Class<T> clazz;

        public RecordSyncedEvent(Class<T> clazz) {
            this.clazz = clazz;
        }

        public Class<T> getClazz() {
            return clazz;
        }
    }

    public static class NoNetworkEvent {
    }

    public static class SyncFailedEvent {
        private final String msg;

        public SyncFailedEvent(String msg) {
            this.msg = msg;
        }

        public String getMsg() {
            return msg;
        }
    }

    @SuppressWarnings("unused")
    public static class SyncRequest<T extends Record> {
        private final Class<T> clazz;
        private SearchRequest request;
        private final boolean userRequest;

        public SyncRequest(Class<T> clazz, SearchRequest request,
                           boolean userRequest) {
            this.clazz = clazz;
            this.request = request;
            this.userRequest = userRequest;
        }

        public SyncRequest(Class<T> clazz, boolean userRequest) {
            this.clazz = clazz;
            this.userRequest = userRequest;
        }

        public SyncRequest(Class<T> clazz) {
            this.clazz = clazz;
            this.userRequest = false;
        }

        public boolean isUserRequest() {
            return userRequest;
        }

        public Class<T> getClazz() {
            return clazz;
        }

        public SearchRequest getRequest() {
            return request;
        }

        public void setRequest(SearchRequest request) {
            this.request = request;
        }
    }


    public static class NotLoggedInEvent {
    }
}

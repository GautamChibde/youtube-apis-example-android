package com.gts.infra.net.http.api.request;

import android.util.Log;


import com.gts.infra.net.http.api.response.Response;
import com.gts.infra.net.http.cache.Cache;
import com.gts.util.Crypt;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;

public class EncodedGsonRequest<Rq,Rs> extends GsonRequest<Rq,Rs> {

    private final String secret;

    public EncodedGsonRequest(int method, String url, Rq requestBody,
                              Response.Listener<Rs> listener,
                              Response.ErrorListener errorListener, Type type, String secret) {
        super(method, url, requestBody, listener, errorListener, type);
        this.secret = secret;
        String data = gson.toJson(requestBody);
        if (requestBody != null) {
            Log.i("EncodedGsonRequest", "data : " + data + "secret : " + secret);
            String encrypted = Crypt.encrypt(data, secret);
            Log.i("EncodedGsonRequest", "Identity : " + Crypt.decrypt(encrypted, secret));
            mRequestBody = gson.toJson(new Payload(encrypted));
        }
    }

    @Override
    protected Response<Rs> getResponse(String responseString, Cache.Entry cacheHeaders)
            throws UnsupportedEncodingException {
        String decoded = Crypt.decrypt(responseString, secret);
        return super.getResponse(decoded, cacheHeaders);
    }

    private class Payload {
        private final String data;

        Payload(String data) {
            this.data = data;
        }

        public String getData() {
            return data;
        }
    }
}

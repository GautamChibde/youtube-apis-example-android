package com.gts.ui.view.component.control.view;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.util.Log;
import android.widget.DatePicker;

import com.gts.infra.orm.base.Model;
import com.gts.ui.view.component.control.listener.ActionListener;
import com.gts.util.Constant;

import java.util.Calendar;
import java.util.Date;

public class DateInput<T extends Model.DateModel>
        extends DialogFragment implements ModelView<T> {

    private T model;
    private ActionListener<Model.DateModel> listener;
    private DatePickerDialog pickerDialog;
    private Date minDate;
    private Date maxDate;

    @Override

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pickerDialog = new DatePickerDialog(
                getActivity(),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        if (listener != null) {
                            Calendar selectedDateCal = Calendar.getInstance();
                            selectedDateCal.set(Calendar.YEAR, year);
                            selectedDateCal.set(Calendar.MONTH, monthOfYear);
                            selectedDateCal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                            listener.onSubmit(new Model.DateModel(selectedDateCal.getTime()));
                        } else {
                            Log.w("DateInput", "Set a listener argument with key PICKER_LISTENER ");
                        }

                    }
                },
                0, 0, 0
        );
        initControl();
    }

    @SuppressWarnings("unchecked")
    void initControl() {
        if (getArguments() == null) {
            throw new RuntimeException("Args cannot be null");
        }
        listener = (ActionListener<Model.DateModel>)
                getArguments().getSerializable(Constant.PICKER_LISTENER);
        model = (T) getArguments().getSerializable(Constant.MODEL);
        initPicker();
    }

    private void initPicker() {
        setModel(model);
        setMinDate(minDate);
        setMaxDate(maxDate);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        initControl();
        return pickerDialog;
    }

    @Override
    public void onStart() {
        initControl();
        super.onStart();
    }

    @Override
    public void setModel(T model) {
        this.model = model;
        if (pickerDialog != null) {
            if (model.getValue() == null) {
                Calendar now = Calendar.getInstance();
                pickerDialog.updateDate(
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
            } else {
                pickerDialog.updateDate(
                        model.getField(Calendar.YEAR),
                        model.getField(Calendar.MONTH),
                        model.getField(Calendar.DAY_OF_MONTH)
                );
            }
        }
    }


    @Override
    public T getModel() {
        return model;
    }

    public void setMinDate(Date minDate) {
        this.minDate = minDate;
        if (pickerDialog != null && minDate != null) {
            pickerDialog.getDatePicker().setMinDate(
                    minDate.getTime()
            );
        }
    }

    public void setMaxDate(Date maxDate) {
        this.maxDate = maxDate;
        if (pickerDialog != null && maxDate != null) {
            pickerDialog.getDatePicker().setMaxDate(
                    maxDate.getTime()
            );
        }
    }
}

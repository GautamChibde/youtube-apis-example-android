package com.gts.infra.orm.base;

import com.gts.infra.orm.dsl.Column;
import com.gts.infra.orm.record.Record;
import com.gts.infra.orm.repo.BaseRepo2;

public abstract class BaseValue implements Record {

    private static final Long serialVersionUID = 1L;

    @Column(name = "state")
    protected String state = "new";

    public void save() {
        BaseRepo2.save(this);
    }

    public boolean isArchived() {
        return "archived".equalsIgnoreCase(state);
    }

    public boolean isNew() {
        return "new".equalsIgnoreCase(state);
    }

    public boolean isMutated() {
        return "mutated".equalsIgnoreCase(state);
    }

    public boolean isActive() {
        return "active".equalsIgnoreCase(state);
    }

    public boolean isValid() {
        return true;
    }

    protected void saveRecord(Record... records) {
        for (Record record : records) {
            saveRecord(record, false);
        }
    }

    protected void saveRecord(Record record, boolean setId) {
        if (record != null) {
            if (setId) {
                record.setId(getId());
            }
            record.save();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BaseValue be = (BaseValue) o;

        return !(getId() != null ? !getId().equals(be.getId()) : be.getId() != null);

    }

    @Override
    public int hashCode() {
        return getId() != null ? getId().hashCode() : 0;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}

package com.gts.infra.net.http.api.request;


import android.util.Log;

import com.gts.infra.auth.AuthFailureError;
import com.gts.infra.net.http.api.response.Response;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

public class AuthGsonRequest<Rq, Rs> extends GsonRequest<Rq, Rs> {

    private static final String TAG = AuthGsonRequest.class.getName();
    private Map<String, String> headers = new HashMap<>();

    public AuthGsonRequest(int method, String url, Rq requestBody,
                           Response.Listener<Rs> listener,
                           Response.ErrorListener errorListener,
                           Type type, String token) {
        super(method, url, requestBody, listener, errorListener, type);
        if (token != null) {
            headers.put("Authorization", "Bearer " + token);
        } else {
            Log.d(TAG, "Token is null");
        }

    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return headers;
    }
}

package com.whizkidz.model;

import android.support.annotation.NonNull;

import com.gts.infra.orm.SugarRecord;
import com.gts.infra.orm.dsl.Column;
import com.gts.infra.orm.dsl.Table;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("unused")
@Table(name = "history")
public class History implements Serializable, Comparable<History> {
    private static final Long serialVersionUID = 1L;

    private Long id;

    @Column(name = "item_id")
    private Data item;

    @Column(name = "count")
    private int count;

    public History() {
    }

    public History(Data item, int count) {
        this.item = item;
        this.count = count;
    }

    @Override
    public String toString() {
        return "History{" +
                "id=" + id +
                ", count=" + count +
                ", item=" + item +
                '}';
    }

    public void save() {
        SugarRecord.save(this);
    }

    public static void delete(History h) {
        SugarRecord.delete(h);
    }

    public static void delete() {
        SugarRecord.deleteAll(History.class);
    }

    public static History containsItem(Data item) {
        List<History> h = SugarRecord.find(History.class, "item_id = ?", String.valueOf(item.getId()));
        if (h.size() != 0) {
            return h.get(0);
        } else {
            return null;
        }
    }

    public static List<History> getAll() {
        return SugarRecord.listAll(History.class);
    }

    public Data getItem() {
        return item;
    }

    public void setItem(Data item) {
        this.item = item;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int compareTo(@NonNull History another) {
        return this.count == another.count ? 0 : (this.count > another.count ? -1 : 1);
    }
}

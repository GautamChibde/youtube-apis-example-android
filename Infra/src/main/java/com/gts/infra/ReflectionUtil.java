package com.gts.infra;

import com.gts.infra.cache.ObjectCache;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Type;

public class ReflectionUtil {

    public static Object invoke(Class<?> clazz,
                                String name,
                                Class[] paramClasses,
                                Object... params) {
        Method toInvoke = getMethod(clazz, name, paramClasses);
        try {
            return toInvoke.invoke(null, params);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static Object invoke(Class<?> clazz, String name) {
        return invoke(clazz, name, null);
    }

    private static Method getMethod(Class<?> clazz, String name, Class... paramClasses) {
        String cachedMethodKey = clazz.getName() + "-" + name;
        Method method = (Method) ObjectCache.get(cachedMethodKey);
        if (method == null) {
            try {
                if (null != paramClasses) {
                    method = clazz.getDeclaredMethod(name, paramClasses);
                } else {
                    method = clazz.getDeclaredMethod(name);
                }
            } catch (Exception e) {
                throw new RuntimeException();
            }
        }
        ObjectCache.put(cachedMethodKey, method);
        return method;
    }

    public static Object getValue(Class<?> clazz, String fieldName) {
        try {
            Field f = clazz.getField(fieldName);
            return f.get(null);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static String getString(Class<?> clazz, String fieldName) {
        return (String) getValue(clazz, fieldName);
    }

    public static int getInteger(Class<?> clazz, String fieldName) {
        return (int) getValue(clazz, fieldName);
    }

    public static Type getType(Class<?> clazz, String fieldName) {
        return (Type) getValue(clazz, fieldName);
    }

    public static <T> T getInstance(Class<T> clazz) {
        try {
            return clazz.newInstance();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private ReflectionUtil() {
        throw new IllegalStateException("Utility class. Do not instantiate");
    }
}

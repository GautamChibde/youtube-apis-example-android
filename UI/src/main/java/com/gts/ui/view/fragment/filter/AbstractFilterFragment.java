package com.gts.ui.view.fragment.filter;

import android.os.Bundle;

import com.gts.infra.cache.ObjectCache;
import com.gts.ui.view.adapter.recycler.AbstractRecyclerAdapter;
import com.gts.ui.view.fragment.AbstractFragment;
import com.gts.ui.view.model.Filter;

import java.util.Set;

@SuppressWarnings("unchecked")
public abstract class AbstractFilterFragment<T> extends AbstractFragment {

    protected Set<Filter<T>> filters;

    private Filter<T> classFilter = new Filter<>(getCode(), null, null);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        filters = (Set<Filter<T>>) ObjectCache.get(getCacheFilterKey());
    }

    protected void addFilter(Filter<T> filter) {
        post(new AbstractRecyclerAdapter.FilterEvent(
                filter,
                AbstractRecyclerAdapter.FilterAction.ADD));
    }

    protected void removeFilter() {
        post(new AbstractRecyclerAdapter.FilterEvent(
                classFilter,
                AbstractRecyclerAdapter.FilterAction.REMOVE));
    }

    protected boolean isActive() {
        return filters != null && filters.contains(classFilter);
    }

    protected Filter<T> getFilter() {
        if (filters != null) {
            for (Filter filter : filters) {
                if (filter.equals(classFilter)) {
                    return filter;
                }
            }
        }
        return null;
    }

    protected abstract String getCode();

    protected abstract String getCacheFilterKey();

    public abstract String getTitle();
}

package com.gts.infra.orm;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import android.util.Log;

import com.gts.infra.orm.dsl.Column;
import com.gts.infra.orm.dsl.NotNull;
import com.gts.infra.orm.dsl.Unique;
import com.gts.infra.orm.dsl.UniqueComposite;
import com.gts.infra.orm.util.NamingHelper;
import com.gts.infra.orm.util.NumberComparator;
import com.gts.infra.orm.util.QueryBuilder;
import com.gts.infra.orm.util.ReflectionUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.gts.infra.orm.util.ReflectionUtil.getDomainClasses;

public class SchemaGenerator {

    private static final String TAG = SchemaGenerator.class.getSimpleName();

    private Context context;

    public SchemaGenerator(Context context) {
        this.context = context;
    }

    public void createDatabase(SQLiteDatabase sqLiteDatabase) {
        for (Class domain : getDomainClasses(context)) {
            createTable(domain, sqLiteDatabase);
        }
    }

    public void doUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        String sql = "select count(*) from sqlite_master where type='table' and name='%s';";
        for (Class domain : getDomainClasses(context)) {
            Cursor c = sqLiteDatabase.rawQuery(String.format(sql, NamingHelper.toSQLName(domain)), null);
            if (c.moveToFirst() && c.getInt(0) == 0) {
                createTable(domain, sqLiteDatabase);
            }
            c.close();
        }
        executeSugarUpgrade(sqLiteDatabase, oldVersion, newVersion);
    }

    public void deleteTables(SQLiteDatabase sqLiteDatabase) {
        List<Class> tables = getDomainClasses(context);
        for (Class table : tables) {
            sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + NamingHelper.toSQLName(table));
        }
    }

    private boolean executeSugarUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        boolean isSuccess = false;
        try {
            List<String> files = Arrays.asList(this.context.getAssets().list("sugar_upgrades"));
            Collections.sort(files, new NumberComparator());
            for (String file : files) {
                Log.i(TAG, "filename : " + file);
                try {
                    int version = Integer.valueOf(file.replace(".sql", ""));

                    if ((version > oldVersion) && (version <= newVersion)) {
                        executeScript(db, file);
                        isSuccess = true;
                    }
                } catch (NumberFormatException e) {
                    Log.i(TAG, "not a sugar script. ignored." + file);
                }

            }
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
        }
        return isSuccess;
    }

    private void executeScript(SQLiteDatabase db, String file) {
        try {
            InputStream is = this.context.getAssets().open("sugar_upgrades/" + file);
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            String line;
            while ((line = reader.readLine()) != null) {
                Log.i("Sugar script", line);
                db.execSQL(line);
            }
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
        }

        Log.i(TAG, "Script executed");
    }

    private void createTable(Class<?> table, SQLiteDatabase sqLiteDatabase) {
        Log.i(TAG, "Create table");
        List<String> uniqueCompositeColumns = new ArrayList<>();
        List<Field> fields = ReflectionUtil.getTableFields(table);
        String tableName = NamingHelper.toSQLName(table);
        StringBuilder sb = new StringBuilder("CREATE TABLE ");
        sb.append(tableName).append(" ( ID INTEGER PRIMARY KEY AUTOINCREMENT ");
        for (Field column : fields) {
            String columnName = NamingHelper.toSQLName(column);
            String columnType = QueryBuilder.getColumnType(column.getType());
            if ("Id".equalsIgnoreCase(columnName)) {
                continue;
            }
            if (column.isAnnotationPresent(Column.class)) {
                Column columnAnnotation = column.getAnnotation(Column.class);
                columnName = columnAnnotation.name();
                sb.append(", ").append(columnName).append(" ").append(columnType);
                if (columnAnnotation.notNull()) {
                    if (columnType.endsWith(" NULL")) {
                        sb.delete(sb.length() - 5, sb.length());
                    }
                    sb.append(" NOT NULL");
                }
                if (columnAnnotation.unique()) {
                    sb.append(" UNIQUE");
                }
                if (column.isAnnotationPresent(UniqueComposite.class)) {
                    uniqueCompositeColumns.add(columnName);
                }
            } else {
                sb.append(", ").append(columnName).append(" ").append(columnType);
                if (column.isAnnotationPresent(NotNull.class)) {
                    if (columnType.endsWith(" NULL")) {
                        sb.delete(sb.length() - 5, sb.length());
                    }
                    sb.append(" NOT NULL");
                }
                if (column.isAnnotationPresent(Unique.class)) {
                    sb.append(" UNIQUE");
                }
            }
        }
        if (uniqueCompositeColumns.size() > 0) {
            sb.append(" , UNIQUE( ")
                    .append(TextUtils.join(",", uniqueCompositeColumns))
                    .append(" ) ON CONFLICT IGNORE ");
        }
        sb.append(" ) ");
        Log.i(TAG, "Creating table " + tableName + " ; ddl: " + sb.toString());
        if (!"".equals(sb.toString())) {
            try {
                sqLiteDatabase.execSQL(sb.toString());
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

}

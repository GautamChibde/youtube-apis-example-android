package com.gts.ui.view.adapter.pager;

import android.app.Fragment;
import android.app.FragmentManager;

import com.gts.ui.view.fragment.filter.AbstractFilterFragment;

import java.util.List;

public class FilterPagerAdapter<T> extends
        AbstractPagerAdapter<AbstractFilterFragment<T>> {

    public FilterPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return pages.get(position).getTitle();
    }

    @Override
    protected CharSequence getTitle(AbstractFilterFragment<T> tAbstractFilterFragment) {
        return null;
    }

    @Override
    protected Fragment getFragment(AbstractFilterFragment<T> tAbstractFilterFragment) {
        return null;
    }

    @Override
    protected List<AbstractFilterFragment<T>> getData() {
        return null;
    }
}

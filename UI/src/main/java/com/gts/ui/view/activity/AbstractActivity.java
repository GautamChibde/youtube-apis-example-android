package com.gts.ui.view.activity;

import android.accounts.AccountManager;
import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.gts.infra.App;
import com.gts.infra.ReflectionUtil;
import com.gts.infra.data.SearchRequest;
import com.gts.infra.net.websocket.service.SocketConnectionManager;
import com.gts.infra.orm.base.BaseEntity;
import com.gts.infra.orm.doc.Store;
import com.gts.infra.orm.repo.BaseRepo2;
import com.gts.ui.R;
import com.gts.infra.event.LoadViewEvent;
import com.gts.ui.event.PermissionGranted;
import com.gts.ui.view.controller.UpPanelManager;
import com.gts.ui.view.fragment.drawer.NavigationDrawer;
import com.gts.util.Constant;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;


/**
 * Parent Activity for all activities in the application
 * Provides methods for working with most infrastructure
 **/
@SuppressWarnings("unused")
public abstract class AbstractActivity extends AppCompatActivity implements AppActivity {

    private static final String TAG = AbstractActivity.class.getSimpleName();

    protected final EventBus bus = EventBus.getDefault();
    protected Toolbar toolbar;
    protected TextView tvTitle;
    protected AccountManager accountManager;
    protected UpPanelManager upPanelManager;
    private Store store;

    // Lifecycle methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerListener(this);
        accountManager = AccountManager.get(getBaseContext());
        store = getApp().getStore();
        if (getLayoutId() != 0) {
            setContentView(getLayoutId());
            setupDrawer();
            setupToolbar();
            replaceContent(getInitContent());
            setTitle("");
            if (getUpPanelFragment() != null) {
                initUpPanel(
                        R.id.upPanelLayout,
                        R.id.upPanelContainer,
                        getUpPanelFragment() != null ?
                                getUpPanelFragment() :
                                new Fragment(),
                        getUpPanelState()
                );
                upPanelManager.setTouchEnabled(false);
            }
        } else {
            if (getInitContent() != null) {
                Log.d(TAG, "Override getLayoutId() to set layout");
            }
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (hasDrawer()) {
            post(new NavigationDrawer.SyncDrawerEvent());
        }
    }

    @Override
    public void onBackPressed() {
        if (upPanelManager != null &&
                upPanelManager.isState(
                        SlidingUpPanelLayout.PanelState.EXPANDED
                )) {
            upPanelManager.setState(onBackPanelState());
        } else if (null != getDrawerLayout() &&
                getDrawerLayout().isDrawerOpen(GravityCompat.START)) {
            getDrawerLayout().closeDrawer(GravityCompat.START);
        } else {
            if (!getFragmentManager().popBackStackImmediate()) {
                supportFinishAfterTransition();
            }
        }
    }

    @Override
    protected void onDestroy() {
        unRegisterListener(this, upPanelManager);
        super.onDestroy();
    }

    // Auth

    public boolean hasClientToken() {
        return getApp().getFromPref(Constant.KEY_CLIENT_TOKEN) != null;
    }

    public String getClientToken() {
        return getApp().getFromPref(Constant.KEY_CLIENT_TOKEN);
    }

    public void requestAuth(Class<? extends Activity> authActivityClass) {
        startActivityForResult(new Intent(this, authActivityClass), Constant.AUTH_REQUEST_CODE);
    }

    public boolean isLoggedIn() {
        return getApp().getFromPref(Constant.KEY_USER_TOKEN) != null;
    }

    protected Long getLoggedInUserId() {
        return getApp().getSharedPref().getLong(Constant.KEY_USER_ID, 0);
    }

    public boolean isSuperAdmin() {
        return "super_admin".equals(getApp().getFromPref(Constant.KEY_USER_ROL));
    }

    public boolean isAdmin() {
        return "admin".equals(getApp().getFromPref(Constant.KEY_USER_ROL));
    }

    public void signOut() {
        getApp().saveToPref(Constant.KEY_USER_TOKEN, null);
    }

    // UI

    protected boolean viewExists(int id) {
        return findViewById(id) != null;
    }

    protected boolean hasDrawer() {
        return viewExists(R.id.drawer);
    }

    protected boolean hasToolbar() {
        return viewExists(R.id.toolbar);
    }

    protected void setupDrawer() {
        int drawerId = getDrawerFragmentId();
        if (hasDrawer()) {
            bus.post(
                    new NavigationDrawer.SetupDrawerEvent(
                            getDrawerLayout(),
                            drawerId
                    )
            );
        }
    }

    protected void setupToolbar() {
        if (hasToolbar()) {
            toolbar = (Toolbar) findViewById(getToolbarId());
            if (toolbar.findViewById(getTitleId()) != null) {
                tvTitle = (TextView) toolbar.findViewById(getTitleId());
            }
            setSupportActionBar(toolbar);
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null && displayHomeButton()) {
                actionBar.setDisplayHomeAsUpEnabled(true);
                actionBar.setHomeButtonEnabled(true);
            }
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        if (tvTitle != null) {
            super.setTitle("");
            tvTitle.setText(title);
        } else {
            super.setTitle(title);
        }
    }

    protected SlidingUpPanelLayout.PanelState onBackPanelState() {
        return SlidingUpPanelLayout.PanelState.HIDDEN;
    }

    protected Fragment getUpPanelFragment() {
        return null;
    }

    protected SlidingUpPanelLayout.PanelState getUpPanelState() {
        return SlidingUpPanelLayout.PanelState.HIDDEN;
    }

    protected void initUpPanel(int layoutId, int panelId, Fragment panelContent) {
        addCartFragment(panelId, panelContent);
        upPanelManager = new UpPanelManager((SlidingUpPanelLayout) findViewById(layoutId));
        upPanelManager.setState(SlidingUpPanelLayout.PanelState.HIDDEN);
        registerListener(upPanelManager);
    }

    protected void initUpPanel(SlidingUpPanelLayout slidingUpPanelLayout,
                               int panelId, Fragment panelContent,
                               SlidingUpPanelLayout.PanelState initState) {
        addCartFragment(panelId, panelContent);
        upPanelManager = new UpPanelManager(slidingUpPanelLayout);
        upPanelManager.setState(initState);
        registerListener(upPanelManager);
    }

    protected void initUpPanel(int layoutId, int panelId, Fragment panelContent,
                               SlidingUpPanelLayout.PanelState initState) {
        initUpPanel(layoutId, panelId, panelContent);
        upPanelManager.setState(initState);
    }

    protected void replaceContent(Class newFragmentClass, Bundle data) {
        try {
            Fragment frag = (Fragment) newFragmentClass.newInstance();
            frag.setArguments(data);
            replaceContent(frag, false);
        } catch (Exception e) {
            Log.d(TAG, "Got error: " + e.getMessage());
        }
    }

    protected void replaceContent(Fragment newFragment) {
        int containerId = getContentContainerId();
        if (containerId != 0 && newFragment != null) {
            getFragmentManager()
                    .beginTransaction()
                    .replace(containerId, newFragment, newFragment.getClass().getName())
                    .commit();
        }
    }

    protected void replaceContent(Fragment newFragment, boolean addToBackStack) {
        if (addToBackStack) {
            int containerId = getContentContainerId();
            if (containerId != 0 && newFragment != null) {
                getFragmentManager()
                        .beginTransaction()
                        .replace(containerId, newFragment, newFragment.getClass().getName())
                        .addToBackStack(newFragment.getClass().getName())
                        .commit();
            }
        } else {
            replaceContent(newFragment);
        }
    }

    private void replaceContent(Class newFragmentClass, Bundle data,
                                boolean addToBackStack) {
        try {
            Fragment frag = (Fragment) newFragmentClass.newInstance();
            frag.setArguments(data);
            replaceContent(frag, addToBackStack);
        } catch (Exception e) {
            Log.d(TAG, "Got error: " + e.getMessage());
        }
    }

    protected int getLayoutId() {
        return 0;
    }

    protected void setFullScreen() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    protected int getDrawerFragmentId() {
        return R.id.drawer;
    }


    protected int getToolbarId() {
        return R.id.toolbar;
    }

    protected Fragment getInitContent() {
        return null;
    }

    protected int getContentContainerId() {
        return R.id.container;
    }

    protected DrawerLayout getDrawerLayout() {
        if (findViewById(R.id.drawer_layout) != null) {
            return (DrawerLayout) findViewById(R.id.drawer_layout);
        }
        return null;
    }

    protected int getTitleId() {
        return R.id.tvTitle;
    }

    protected boolean displayHomeButton() {
        return true;
    }

    private void addCartFragment(int cartContainerId, Fragment cartContentFragment) {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.replace(cartContainerId, cartContentFragment).commit();
    }

    public void showDialog(DialogFragment dialogFragment, Bundle dataBundle, String tag) {
        dialogFragment.setArguments(dataBundle);
        dialogFragment.show(getFragmentManager(), tag);
    }

    public void showDialog(Class<DialogFragment> dialogFragmentClass, Bundle dataBundle, String tag) {
        DialogFragment dialogFragment = ReflectionUtil.getInstance(dialogFragmentClass);
        dialogFragment.setArguments(dataBundle);
        dialogFragment.show(getFragmentManager(), tag);
    }


    // Comm

    public boolean isNetworkConnected() {
        return getApp().getNetworkManager().isConnected();
    }

    protected void post(Object event) {
        bus.post(event);
    }

    protected void startNewActivity(Class clazz) {
        startNewActivity(clazz, new Bundle());
    }

    protected void startNewActivity(Class clazz, Bundle data) {
        Intent intent = new Intent(this, clazz);
        intent.putExtras(data);
        if (data.getBoolean(Constant.FOR_RESULT)) {
            startActivityForResult(intent, data.getInt(Constant.REQUEST_CODE));
        } else {
            startActivity(intent);
        }
    }

    protected void startService(Class<? extends Service> clazz) {
        startService(new Intent(this, clazz));
    }

    protected void startServiceSocketManager() {
        startService(new Intent(this, SocketConnectionManager.class));
    }

    protected void startServiceSocketManager(Class<? extends SocketConnectionManager> serviceClass) {
        startService(new Intent(this, serviceClass));
    }

    protected void registerListenerSticky(Object... listeners) {
        getApp().registerListener(listeners);
    }

    protected void unRegisterListener(Object... listeners) {
        getApp().unRegisterListener(listeners);
    }

    protected void registerListener(Object... listeners) {
        getApp().registerListener(listeners);
    }

    public App getApp() {
        if (getApplication() instanceof App) {
            return (App) getApplication();
        } else {
            throw new RuntimeException("Please register a custom application " +
                    "class by overriding App");
        }
    }

    @SuppressWarnings("unchecked")
    public void onEvent(LoadViewEvent event) {
        if (event.getComponent() instanceof Class) {
            Class clazz = (Class) event.getComponent();
            if (DialogFragment.class.isAssignableFrom(clazz)) {
                showDialog(
                        (Class<DialogFragment>) clazz,
                        event.getData(),
                        clazz.getSimpleName()
                );
            } else if (Fragment.class.isAssignableFrom(clazz)) {
                replaceContent(clazz, event.getData());
            }
        } else if (DialogFragment.class.isAssignableFrom(
                event.getComponent().getClass())) {
            showDialog(
                    (DialogFragment) event.getComponent(),
                    event.getData(),
                    event.getComponent().getClass().getSimpleName()
            );
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        post(new PermissionGranted(requestCode, permissions, grantResults));
    }

    public void askPermission(int requestId, String... permissions) {
        List<String> needPerms = new ArrayList<>();
        for (String permission : permissions) {
            if (permission != null && !hasPermission(permission)) {
                needPerms.add(permission);
            }
        }
        ActivityCompat.requestPermissions(
                this,
                needPerms.toArray(new String[needPerms.size()]),
                requestId
        );
    }

    protected boolean hasPermission(String permission) {
        return !TextUtils.isEmpty(permission) &&
                PackageManager.PERMISSION_GRANTED ==
                        ContextCompat.checkSelfPermission(this, permission);
    }

    // Data Sync

    public void sync(Class<? extends BaseEntity> clazz) {
        new SyncTask(clazz, null).execute();
    }

    public void sync(Store.SyncRequest<? extends BaseEntity> syncRequest) {
        new SyncTask(null, syncRequest).execute();
    }

    public class SyncTask extends AsyncTask<Void, Void, Void> {

        private Class<? extends BaseEntity> clazz;
        private Store.SyncRequest<? extends BaseEntity> syncRequest;


        public SyncTask(Class<? extends BaseEntity> clazz,
                        Store.SyncRequest<? extends BaseEntity> syncRequest) {
            this.clazz = clazz;
            this.syncRequest = syncRequest;
        }

        @Override
        protected Void doInBackground(Void... params) {
            if (syncRequest == null) {
                syncRequest = new Store.SyncRequest<>(clazz);
            }
            SearchRequest searchRequest = syncRequest.getRequest();
            if (searchRequest == null) {
                searchRequest = new SearchRequest();
                syncRequest.setRequest(searchRequest);
            }
            searchRequest.setUpdatedStamp(
                    BaseRepo2.getLastUpdatedStamp(clazz)
            );
            store.sync(syncRequest);
            return null;
        }

        @Override
        protected void onPostExecute(Void needsSync) {
        }
    }

    public class InitTask extends AsyncTask<Void, Void, Void> {

        private final boolean recreate;

        public InitTask(boolean recreate) {
            this.recreate = recreate;
        }

        @Override
        protected Void doInBackground(Void... params) {
            getApp().init(recreate);
            return null;
        }

    }
}

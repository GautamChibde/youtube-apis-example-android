package com.gts.ui.view.fragment.auth;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.gts.infra.auth.AuthManager;
import com.gts.ui.R;
import com.gts.ui.view.fragment.AbstractFragment;

public abstract class AbstractAuthFragment extends AbstractFragment {

    protected AuthManager authManager;

    protected EditText etUsername;
    protected EditText etPassword;
    protected EditText etName;
    protected EditText etVerifyPassword;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        authManager = getApp().getAuthManager();
    }

    @Override
    protected void initView(View v) {
        super.initView(v);
        etUsername = (EditText) v.findViewById(R.id.etUsername);
        etPassword = (EditText) v.findViewById(R.id.etPassword);
        etName = (EditText) v.findViewById(R.id.etName);
        etVerifyPassword = (EditText) v.findViewById(R.id.etVerifyPassword);

        v.findViewById(R.id.btnSubmit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dataValid()) {
                    onSubmit();
                }
            }
        });

        showKeyboard(getActivity().getCurrentFocus());
    }

    protected abstract void onSubmit();

    protected abstract boolean dataValid();

}

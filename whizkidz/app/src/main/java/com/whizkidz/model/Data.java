package com.whizkidz.model;

import com.gts.infra.orm.SugarRecord;
import com.gts.infra.orm.dsl.Column;
import com.gts.infra.orm.dsl.Table;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("unused")
@Table(name = "data")
public class Data implements Serializable {
    private static final Long serialVersionUID = 1L;

    private Long id;

    @Column(name = "url")
    private String url;

    @Column(name = "title")
    private String title;

    @Column(name = "min_age")
    private int min_age;

    @Column(name = "desc")
    private String desc;

    private int max_age;

    private String channel;

    private Long num_views;

    private String theme;

    private String sub_theme;

    private Date pub_on_date;

    public Data() {
    }

    public void save() {
        SugarRecord.save(this);
    }

    @Override
    public String toString() {
        return "Data{" +
                "id=" + id +
                ", url='" + url + '\'' +
                ", title='" + title + '\'' +
                ", min_age=" + min_age +
                ", max_age=" + max_age +
                ", desc='" + desc + '\'' +
                ", channel='" + channel + '\'' +
                ", num_views=" + num_views +
                ", theme='" + theme + '\'' +
                ", sub_theme='" + sub_theme + '\'' +
                ", pub_on_date=" + pub_on_date +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getMin_age() {
        return min_age;
    }

    public void setMin_age(int min_age) {
        this.min_age = min_age;
    }

    public int getMax_age() {
        return max_age;
    }

    public void setMax_age(int max_age) {
        this.max_age = max_age;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public Long getNum_views() {
        return num_views;
    }

    public void setNum_views(Long num_views) {
        this.num_views = num_views;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public String getSub_theme() {
        return sub_theme;
    }

    public void setSub_theme(String sub_theme) {
        this.sub_theme = sub_theme;
    }

    public Date getPub_on_date() {
        return pub_on_date;
    }

    public void setPub_on_date(Date pub_on_date) {
        this.pub_on_date = pub_on_date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}

package com.gts.ui.view.activity.home;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.gts.ui.view.activity.AbstractActivity;
import com.gts.ui.view.activity.BaseAuthActivity;
import com.gts.ui.view.fragment.drawer.NavigationDrawer;

public abstract class AbstractHomeActivity extends AbstractActivity {

    protected boolean skipAuth = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (isLoggedIn() || skipAuth) {
            initView();
        } else {
            requestAuth(getAuthActivityClass());
            unRegisterListener(this);
            finish();
        }
    }

    public void onEvent(NavigationDrawer.DrawerItemClickedEvent event) {
        startNewActivity(
                event.getLink().getViewToShow(),
                getDataBundle()
        );
    }

    protected Bundle getDataBundle() {
        return new Bundle();
    }

    protected abstract void initView();

    @NonNull
    protected abstract Class<? extends BaseAuthActivity> getAuthActivityClass();

}

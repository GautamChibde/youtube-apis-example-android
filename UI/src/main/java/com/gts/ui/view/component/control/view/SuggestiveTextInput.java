package com.gts.ui.view.component.control.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;

import com.gts.infra.orm.base.Model;
import com.gts.ui.R;
import com.gts.ui.view.adapter.list.AbstractAutoCompleteAdapter;
import com.gts.ui.view.component.control.listener.ActionListener;
import com.gts.util.PlatformUtil;

import java.util.List;

public class SuggestiveTextInput<T extends Model>
        extends AutoCompleteTextView implements ModelView<T> {

    private ActionListener<T> listener;
    private T model;

    public SuggestiveTextInput(Context context) {
        super(context);
    }

    public SuggestiveTextInput(Context context, ActionListener<T> listener) {
        super(context);
        this.listener = listener;
    }

    public SuggestiveTextInput(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public SuggestiveTextInput(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        setThreshold(1);
        setCompoundDrawablePadding(10);
        setCompoundDrawablesWithIntrinsicBounds(
                0, 0, R.drawable.ic_done_black_24dp, 0
        );
        setAdapter(new AbstractAutoCompleteAdapter<T>(getContext()) {

            @Override
            @SuppressWarnings("unchecked")
            protected List<T> performFiltering(CharSequence constraint) {
                return listener.onSuggestiveTextInput(constraint.toString());
            }

            @Override
            protected String getLabel(T aValue) {
                return aValue.getLabel();
            }
        });
        setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (getRight() - 60)) {
                        if (model instanceof Model.StringModel) {
                            listener.onSubmit((T) new Model.StringModel(getText().toString()));
                        } else {
                            getText().clear();
                        }
                        return true;
                    }
                }
                return false;
            }
        });
        setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {
                        PlatformUtil.hideKeyboard(getContext(), SuggestiveTextInput.this);
                        listener.onSubmit((T) parent.getAdapter().getItem(position));
                    }
                }
        );
    }

    public void setModel(T model) {
        this.model = model;
        if (this.model != null) {
            setText(model.getLabel());
        }
    }

    public T getModel() {
        return model;
    }
}

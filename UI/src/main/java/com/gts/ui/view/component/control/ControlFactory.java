package com.gts.ui.view.component.control;

import android.content.Context;

import com.gts.infra.orm.base.Model;
import com.gts.ui.view.component.control.listener.ActionListener;

import java.util.Date;
import java.util.List;

@SuppressWarnings("unused")
public class ControlFactory {

    private static <T extends Model> void setBaseFields(
            BaseControl<T> control, String label, ActionListener<T> listener) {
        control.setLabel(label);
        control.setListener(listener);
    }

    public static <T extends Model> TextControl<T> buildTextControl(
            Context context, String label,
            ActionListener<T> listener
    ) {
        TextControl<T> control = new TextControl<>(context);
        setBaseFields(control, label, listener);
        return control;
    }

    public static <T extends Model> SpinnerControl<T> buildSpinnerControl(
            Context context, String label,
            ActionListener<T> listener, List<T> items
    ) {
        SpinnerControl<T> control = new SpinnerControl<>(context);
        setBaseFields(control, label, listener);
        control.setItems(items);
        return control;
    }

    public static DateControl buildDateControl(
            Context context, String label,
            ActionListener<Model.DateModel> listener,
            Date minDate, Date maxDate
    ) {
        DateControl control = new DateControl(context);
        setBaseFields(control, label, listener);
        control.setMinDate(minDate);
        control.setMaxDate(maxDate);
        return control;
    }

    public static TimeControl buildTimeControl(
            Context context, String label,
            ActionListener<Model.TimeModel> listener
    ) {
        TimeControl control = new TimeControl(context);
        setBaseFields(control, label, listener);
        return control;
    }
}

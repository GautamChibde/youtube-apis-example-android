package com.gts.ui.view.component.control.view;

public interface ModelView<T> {

    T getModel();

    void setModel(T model);

}

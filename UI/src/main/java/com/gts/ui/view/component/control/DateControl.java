package com.gts.ui.view.component.control;

import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Toast;

import com.gts.infra.orm.base.Model;
import com.gts.infra.event.LoadViewEvent;
import com.gts.ui.view.activity.AbstractActivity;
import com.gts.ui.view.component.control.listener.ActionListener;
import com.gts.ui.view.component.control.view.DateInput;
import com.gts.ui.view.component.control.view.ReadOnlyText;
import com.gts.util.Constant;

import java.util.Date;

public class DateControl extends BaseControl<Model.DateModel> {

    private DateInput<Model.DateModel> dateInput = new DateInput<>();

    public DateControl(Context context) {
        super(context);
    }

    public DateControl(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected View getEditView() {
        return new ReadOnlyText<Model.DateModel>(getContext()) {
            @Override
            protected void onClick() {
            }
        };
    }

    @Override
    protected void onEditClick() {
        //super.onEditClick();
        Bundle args = new Bundle();
        args.putSerializable(
                Constant.PICKER_LISTENER,
                new ActionListener.SimpleListener<Model.DateModel>() {
                    @Override
                    public void onSubmit(Model.DateModel newValue) {
                        if (listener.isValid(newValue)) {
                            listener.onSubmit(newValue);
                            setModel(newValue);
                        }
                    }
                });
        args.putSerializable(Constant.MODEL, getModel());
        bus.post(new LoadViewEvent(
                dateInput, args
        ));
        ((AbstractActivity) getContext()).showDialog(
                dateInput, args, DateInput.class.getName()
        );
    }

    @Override
    protected void validationFailed() {
        Toast.makeText(getContext(), "Invalid date", Toast.LENGTH_LONG).show();
    }


    public void setMinDate(Date minDate) {
        if (dateInput != null && minDate != null) {
            dateInput.setMinDate(minDate);
        }
    }

    public void setMaxDate(Date maxDate) {
        if (dateInput != null && maxDate != null) {
            dateInput.setMaxDate(maxDate);
        }
    }
}

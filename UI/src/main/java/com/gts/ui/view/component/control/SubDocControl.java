package com.gts.ui.view.component.control;

import android.app.DialogFragment;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import java.util.List;

public class SubDocControl<T> extends LinearLayout {

    private List<T> model;

    public SubDocControl(Context context) {
        super(context);
    }

    public SubDocControl(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SubDocControl(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();

    }

    public void setModel(List<T> model) {
        this.model = model;
    }

    public List<T> getModel() {
        return model;
    }

    private DialogFragment getEditView(){
        return null;
    }

    private void onEditClick(){

    }
}

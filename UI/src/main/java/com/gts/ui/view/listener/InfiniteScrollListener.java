package com.gts.ui.view.listener;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.gts.ui.view.adapter.recycler.AbstractRecyclerAdapter;

public abstract class InfiniteScrollListener
        extends RecyclerView.OnScrollListener {

    private final AbstractRecyclerAdapter adapter;
    protected int threshold;
    private final LinearLayoutManager layoutManager;


    public InfiniteScrollListener(LinearLayoutManager layoutManager,
                                  AbstractRecyclerAdapter adapter) {
        this.layoutManager = layoutManager;
        this.adapter = adapter;
    }

    public InfiniteScrollListener(LinearLayoutManager layoutManager,
                                  AbstractRecyclerAdapter adapter,
                                  int threshold) {
        this.layoutManager = layoutManager;
        this.adapter = adapter;
        this.threshold = threshold;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        int alreadyLoaded = adapter.getItemCount() - 1;
        synchronized (this) {
            if ((alreadyLoaded - threshold) == layoutManager.findLastVisibleItemPosition()) {
                loadNextBatch();
            }
        }
        super.onScrolled(recyclerView, dx, dy);
    }

    public abstract void loadNextBatch();

}

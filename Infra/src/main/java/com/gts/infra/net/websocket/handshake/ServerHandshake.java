package com.gts.infra.net.websocket.handshake;

public interface ServerHandshake extends Handshakedata {
	short getHttpStatus();
	String getHttpStatusMessage();
}

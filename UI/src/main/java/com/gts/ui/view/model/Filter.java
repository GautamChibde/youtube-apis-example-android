package com.gts.ui.view.model;

import com.gts.util.IPredicate;

import java.io.Serializable;

public class Filter<T> implements Serializable {

    private final String code;
    private final IPredicate<T> predicate;
    private final Object viewModel;


    public Filter(String code, IPredicate<T> predicate, Object viewModel) {
        this.code = code;
        this.predicate = predicate;
        this.viewModel = viewModel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Filter<?> filter = (Filter<?>) o;

        return code.equals(filter.code);

    }

    @Override
    public int hashCode() {
        return code.hashCode();
    }

    public String getCode() {
        return code;
    }

    public IPredicate<T> getPredicate() {
        return predicate;
    }

    public Object getViewModel() {
        return viewModel;
    }
}

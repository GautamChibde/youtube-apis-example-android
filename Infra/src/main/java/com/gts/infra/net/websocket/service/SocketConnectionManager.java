package com.gts.infra.net.websocket.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.gts.infra.App;
import com.gts.infra.net.websocket.client.AppSocketClient;
import com.gts.infra.net.websocket.client.WebSocketClient;

import java.net.URI;
import java.net.URISyntaxException;

import de.greenrobot.event.EventBus;

public class SocketConnectionManager extends Service {

    private static final String TAG = SocketConnectionManager.class.getName();
    private WebSocketClient client;
    private URI endpointUri;

    protected App getApp() {
        return (App) getApplication();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        try {
//            Log.i(TAG, getApp().getClientToken());
            endpointUri = new URI(getApp().getWebSocketUri());
        } catch (URISyntaxException e) {
            Log.e(TAG, e.getMessage());
            //TODO notify connection error
            throw new RuntimeException(e);
        }
        client = new AppSocketClient(endpointUri);
        getApp().registerListener(this);
        client.connect();
    }

    protected void post(Object event) {
        EventBus.getDefault().post(event);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

//    @SuppressWarnings("unused")
//    public void onEvent(AppSocketClient.SocketOpenedEvent event) {
//        client.send("Hello");
//    }

    @SuppressWarnings("unused")
    public void onEvent(AppSocketClient.SocketClosedEvent event) {
//        getApp().logDebug(
//                TAG,
//                "onEvent(SocketClosedEvent)",
//                "Socket closed. Reason: " + event.getReason() +
//                        " Code: " + event.getCode() +
//                        " Remote: " + event.isRemote());
    }

    @SuppressWarnings("unused")
    public void onEvent(CloseSocketConnection event) {
        client.close();
        client = null;
    }

    @SuppressWarnings("unused")
    public void onEvent(AttemptSocketConnectionEvent event) {
        client = new AppSocketClient(endpointUri);
        client.connect();
    }

    @SuppressWarnings("unused")
    public void onEvent(SendDataEvent event) {
       // getApp().logInfo(TAG, "onEvent(SendDataEvent)", event.getData());
        client.send(event.getData());
    }

    @Override
    public void onDestroy() {
        getApp().unRegisterListener(this);
        client.close();
        super.onDestroy();
    }

    public static class AttemptSocketConnectionEvent {
    }

    public static class CloseSocketConnection {
    }

    public static class SendDataEvent {
        private final String data;

        public SendDataEvent(String data) {
            this.data = data;
        }

        public String getData() {
            return data;
        }
    }
}


//mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//private NotificationManager mNotificationManager;
//private static final Random random = new Random();

//showNotification(event.getData());

//    private void showNotification(String data) {
//        NotificationCompat.Builder mBuilder =
//                new NotificationCompat.Builder(this)
//                        .setSmallIcon(R.mipmap.ic_launcher)
//                        .setContentTitle("New order")
//                        .setContentText(data);
//        Intent sendIntent = new Intent();
//        sendIntent.setAction(Intent.ACTION_SEND);
//        sendIntent.putExtra(Intent.EXTRA_TEXT, data);
//        sendIntent.setType("text/plain");
//        sendIntent.setPackage("com.whatsapp");
//        PendingIntent resultPendingIntent = PendingIntent.getActivity(
//                this,
//                0,
//                sendIntent,
//                PendingIntent.FLAG_ONE_SHOT
//        );
//        mBuilder.setContentIntent(resultPendingIntent);
//        mNotificationManager.notify(random.nextInt(), mBuilder.build());
//
//    }
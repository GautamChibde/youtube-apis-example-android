package com.gts.infra.orm.base;

import com.gts.infra.orm.dsl.Column;
import com.gts.infra.orm.repo.BaseRepo2;

public abstract class BaseValueCodeName extends BaseValueNamed {

    @Column(name = "code")
    protected String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}

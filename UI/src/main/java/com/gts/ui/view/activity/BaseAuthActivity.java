package com.gts.ui.view.activity;

import android.app.Fragment;
import android.text.TextUtils;

import com.gts.infra.auth.Auth;
import com.gts.infra.net.http.Resource;
import com.gts.ui.R;
import com.gts.ui.view.fragment.auth.LoginFragment;
import com.gts.util.Constant;

import java.util.HashMap;
import java.util.Map;

public class BaseAuthActivity extends AbstractActivity {

    @SuppressWarnings("unused")
    public void onEvent(Resource.HttpRequestComplete event) {
        if (Constant.AUTH_REQUEST_CODE == event.getRequestId()) {
            if (null != event.getResponse()
                    && event.getResponse() instanceof Auth) {
                onSuccess((Auth) event.getResponse());
            } else {
                onFailure(event.getMessage());
            }
        }
    }

    protected void onSuccess(Auth auth) {
        if (!TextUtils.isEmpty(auth.getAccess_token())) {
            Map<String, Object> credentials = new HashMap<>();
            credentials.put(Constant.KEY_USER_TOKEN, auth.getAccess_token());
            credentials.put(Constant.KEY_USER_ROL, auth.getRol());
            credentials.put(Constant.KEY_USER_ID, auth.getUser_id());
            getApp().saveToPref(credentials);
        } else {
            onFailure("Empty access token");
        }
    }

    protected void onFailure(String message) {
    }

    @Override
    protected boolean displayHomeButton() {
        return false;
    }

    @Override
    protected Fragment getInitContent() {
        return new LoginFragment();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_container;
    }

    //
//    protected void onFailure(String message) {
//        ViewFactory.getAuthErrorSnackbar(
//                root,
//                "Sign up failed :" + message,
//                android.support.design.R.id.snackbar_text,
//                PlatformUtil.getColor(
//                        getActivity(),
//                        R.color.primary_color_trans
//                ),
//                Color.RED
//        ).show();
//    }
}

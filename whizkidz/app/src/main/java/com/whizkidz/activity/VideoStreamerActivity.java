package com.whizkidz.activity;

import android.os.Bundle;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.gts.util.Constant;
import com.gts.util.PlatformUtil;
import com.whizkidz.R;
import com.whizkidz.model.Data;
import com.whizkidz.model.History;

public class VideoStreamerActivity extends YouTubeBaseActivity
        implements YouTubePlayer.OnInitializedListener {
    private Data data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_streamer);
        data = (Data) getIntent().getSerializableExtra(Constant.MODEL);
        data.save();
        History h = History.containsItem(data);
        if (h != null) {
            h.setCount(1 + h.getCount());
            h.save();
        } else {
            new History(data, 1).save();
        }
        YouTubePlayerView playerView = (YouTubePlayerView) findViewById(R.id.player);
        playerView.initialize(Constant.YT_KEY, this);
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider,
                                        YouTubePlayer youTubePlayer, boolean b) {
        youTubePlayer.loadVideo(PlatformUtil.extractYTId(data.getUrl()));
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider,
                                        YouTubeInitializationResult youTubeInitializationResult) {

    }
}

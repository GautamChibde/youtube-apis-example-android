package com.gts.ui.view.component.control.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.gts.infra.orm.base.Model;
import com.gts.ui.view.adapter.list.AbstractSimpleListItemAdapter;
import com.gts.ui.view.component.control.listener.ActionListener;

import java.util.List;

public class SpinnerInput<T extends Model>
        extends Spinner implements ModelView<T> {

    private List<T> items;
    private ActionListener<T> listener;
    private AbstractSimpleListItemAdapter<T> adapter;
    private T model;

    //TODO Need better way to do this. :(
    boolean fromModelSet = true;

    public SpinnerInput(Context context, List<T> items, ActionListener<T> listener) {
        super(context);
        this.listener = listener;
        this.items = items;
    }

    public SpinnerInput(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SpinnerInput(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        setAdapter(adapter = new AbstractSimpleListItemAdapter<T>(getContext(), items) {

            @Override
            protected String getText(T item) {
                return item.getLabel();
            }
        });
        setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view,
                                               int position, long id) {
                        if (!fromModelSet) {
                            listener.onSubmit((T) parent.getSelectedItem());
                        }
                        fromModelSet = false;
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                }
        );

    }

    public void setListener(ActionListener<T> listener) {
        this.listener = listener;
    }

    @Override
    public T getModel() {
        return model;
    }

    public void setModel(T model) {
        this.model = model;
        int index = items.indexOf(model);
        if (index >= 0
                && index < items.size()
                && index != getSelectedItemId()) {
            fromModelSet = true;
            setSelection(index);
        } else {
            fromModelSet = false;
        }
    }

    public void notifyDataSetChanged() {
        adapter.notifyDataSetChanged();
    }
}

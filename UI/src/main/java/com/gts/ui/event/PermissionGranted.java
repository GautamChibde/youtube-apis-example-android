package com.gts.ui.event;

public class PermissionGranted {

    private final int requestCode;
    private final String[] permissions;
    private final int[] grantResults;


    public PermissionGranted(int requestCode, String[] permissions, int[] grantResults) {
        this.requestCode = requestCode;
        this.permissions = permissions;
        this.grantResults = grantResults;
    }

    public int getRequestCode() {
        return requestCode;
    }

    public String[] getPermissions() {
        return permissions;
    }

    public int[] getGrantResults() {
        return grantResults;
    }
}

package com.gts.ui.view.fragment;


import android.app.Dialog;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.gts.infra.orm.base.BaseTree;
import com.gts.infra.orm.base.BaseValueNamed;
import com.gts.infra.orm.base.Model;
import com.gts.infra.orm.record.Record;
import com.gts.ui.R;
import com.gts.ui.view.adapter.recycler.AbstractRecyclerAdapter;
import com.gts.ui.view.component.control.ControlFactory;
import com.gts.ui.view.component.control.SpinnerControl;
import com.gts.ui.view.component.control.TextControl;
import com.gts.ui.view.component.control.listener.ActionListener;
import com.gts.ui.view.controller.UpPanelManager;

import java.util.List;


@SuppressWarnings("unused")
public abstract class AbstractFormFragment<T extends Record>
        extends AbstractDialogFragment {

    protected T record;
    private long mLastClickTime = 0;

    private enum MODE {
        READ, EDIT, ADD
    }

    @Override
    @SuppressWarnings("unchecked")
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerAsListener();
        if (null != getArguments()) {
            record = (T) getArguments().getSerializable(
                    getClazz().getSimpleName()
            );
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }


    public void onEvent(AbstractRecyclerAdapter.ItemSelectedEvent<T> event) {
        record = event.getEntity();
        if (event.getEntity() != null) {
            setTitle(getTitle());
        }
        initView(getView());
        post(
                new UpPanelManager.TogglePanelEvent(
                        UpPanelManager.STATE.EXPANDED
                )
        );
    }

    protected String getTitle() {
        return "";
    }

    protected void onSubmit() {
        hideKeyboard();
        if (isEdit() && record.isMutated()) {
            getApp().getStore().save(record);
        } else {
            getApp().getStore().save(newRecord());
        }
        close();

    }

    protected boolean isEdit() {
        return record != null && record.getId() != null;
    }

    protected Toolbar.OnMenuItemClickListener getChangeListener(final int actionButtonId) {
        return new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == actionButtonId) {
                    if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                        return true;
                    }
                    mLastClickTime = SystemClock.elapsedRealtime();
                    onSubmit();
                    return true;
                }
                return false;
            }
        };
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
            );
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.view_form;
    }

    protected boolean isFirstRun(View root) {
        return ((LinearLayout) root).getChildCount() == 1;
    }

    protected void addControl(View root, View control) {
        ((LinearLayout) root).addView(control);
    }

    protected abstract Record newRecord();

    protected abstract Class<? extends T> getClazz();

    // standard controls

    protected <C extends BaseValueNamed> TextControl<Model.StringModel> getNameControl(
            final C record
    ) {
        return ControlFactory.buildTextControl(
                getActivity(),
                "Name",
                new ActionListener.SimpleListener<Model.StringModel>() {
                    @Override
                    public void onSubmit(Model.StringModel newValue) {
                        record.setName(newValue.getValue());
                    }
                }
        );
    }

    protected <C extends BaseTree> SpinnerControl<C> getCategoryControl(
            ActionListener<C> listener, List<C> items
    ) {
        return ControlFactory.buildSpinnerControl(
                getActivity(), "Category",
                listener,
                items
        );
    }

    protected <C extends BaseTree> SpinnerControl<C> getParentControl(
            ActionListener<C> listener, List<C> items
    ) {
        return ControlFactory.buildSpinnerControl(
                getActivity(), "Parent",
                listener,
                items
        );
    }

}

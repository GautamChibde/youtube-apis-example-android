package com.gts.ui.view.adapter.list;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


public abstract class InMemoryAutoCompleteAdapter<T> extends AbstractListAdapter<T> {

    private List<T> master;
    private final int backgroundColor;
    private final int textColor;

    public InMemoryAutoCompleteAdapter(Context context, List<T> objects) {
        super(context, objects);
        master = new ArrayList<>(objects);
        this.backgroundColor = Color.WHITE;
        this.textColor = Color.BLACK;
    }

    public InMemoryAutoCompleteAdapter(Context context, List<T> objects,
                                       int backgroundColor, int textColor) {
        super(context, objects);
        master = new ArrayList<>(objects);
        this.backgroundColor = backgroundColor == 0 ? Color.WHITE : backgroundColor;
        this.textColor = textColor == 0 ? Color.BLACK : textColor;
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    @Override
    protected View getView(int position, ViewGroup parent) {
        return inflater.inflate(
                android.R.layout.simple_list_item_1,
                parent,
                false);
    }

    @Override
    protected void initView(View view, int position) {
        TextView tv = (TextView) view;
        tv.setText(getLabel(getItem(position)));
        tv.setBackgroundColor(backgroundColor);
        tv.setTextColor(textColor);
    }

    private Filter mFilter = new Filter() {
        @SuppressWarnings("unchecked")
        @Override
        public String convertResultToString(Object resultValue) {
            return getLabel((T) resultValue);
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            if (constraint != null) {
                ArrayList<T> suggestions = new ArrayList<>();
                for (T aValue : master) {
                    if (getLabel(aValue) != null &&
                            getLabel(aValue).toLowerCase().contains(
                                    constraint.toString().toLowerCase())) {
                        suggestions.add(aValue);
                    }
                }
                results.values = suggestions;
                results.count = suggestions.size();
            }
            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            clear();
            if (results != null && results.count > 0) {
                addAll((ArrayList<T>) results.values);
            } else {
                addAll(master);
            }
            notifyDataSetChanged();
        }
    };

    protected abstract String getLabel(T resultValue);

}

package com.whizkidz.activity;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.reflect.TypeToken;
import com.gts.infra.net.http.Resource;
import com.gts.ui.view.activity.AbstractActivity;
import com.whizkidz.R;
import com.whizkidz.WizApp;
import com.whizkidz.adapter.HistoryAdapter;
import com.whizkidz.adapter.ItemAdapter;
import com.whizkidz.model.Data;
import com.whizkidz.model.History;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@SuppressWarnings("unused,unchecked,ConstantConditions")
public class MainActivity extends AbstractActivity {
    private List<Data> items;
    private ProgressBar pb;
    private RecyclerView rvh;
    private TextView noItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        noItems = (TextView) findViewById(R.id.tv_no_items);
        items = new ArrayList<>();
        pb = (ProgressBar) findViewById(R.id.progress);
        Resource.getAsync(
                getApp(),
                WizApp.url,
                WizApp.requestCode,
                new TypeToken<List<Data>>() {
                }.getType(),
                "data");
        rvh = (RecyclerView) findViewById(R.id.rv_history);
        rvh.setLayoutManager(new LinearLayoutManager(this));
        setHistoryAdapter();
    }

    private void setHistoryAdapter() {
        List<History> h = History.getAll();
        Collections.sort(h);
        if (h.size() != 0) {
            rvh.setVisibility(View.VISIBLE);
            noItems.setVisibility(View.GONE);
            if (h.size() > 4) {
                rvh.setAdapter(new HistoryAdapter(this, h.subList(0, 4)));
            } else {
                rvh.setAdapter(new HistoryAdapter(this, h));
            }
        } else {
            rvh.setVisibility(View.GONE);
            noItems.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setHistoryAdapter();
    }

    public void onEvent(Resource.HttpRequestComplete event) {
        items = (List<Data>) event.getResponse();
        pb.setVisibility(View.GONE);
        initView();
    }

    public void clear(View view) {
        History.delete();
        rvh.setVisibility(View.GONE);
        noItems.setVisibility(View.VISIBLE);
    }

    private void initView() {
        RecyclerView rv = (RecyclerView) findViewById(R.id.recyclerView);
        rv.startAnimation(AnimationUtils.loadAnimation(this, R.anim.animate_left));
        rv.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        rv.setAdapter(new ItemAdapter(this, items));
    }
}
